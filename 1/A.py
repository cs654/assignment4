#!/usr/bin/python

import argparse
import socket
import atexit
import datetime
import time
from threading import Thread

sock_array = []
check_interval = 10

def cleanup():
    print("Closing current sockets...")
    for sock in sock_array:
        sock.close()
    print("Bye.")

def send_data(host, port):
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    sock_array.append(s)
    s.settimeout(5)

    try:
        s.connect((host, port))
        while True:
            try:
                s.send('ping'.encode())
                response = s.recv(1024)
                if response.decode() == 'echo':
                    print('[%s] Machine at %s is \033[1;32mAVAILABLE\033[0m.' 
                        % (datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')
                        , host))
                else:
                    print(response.decode())
                    break                    
            except:
                break
            time.sleep(check_interval)
    except:
        pass

    print('[%s] Machine at %s is \033[1;31mFAULTY\033[0m.' 
        % (datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')
        , host))
    sock_array.remove(s)
    s.close()

def register_parser():
    parser = argparse.ArgumentParser(description='Ping different machines to check availability.')
    parser.add_argument('hosts', metavar='HOST', type=str, nargs='+',
                       help='list of IPs of hosts to ping')

    args = parser.parse_args()
    return args

def send_data_loop(host, port):
    while True:
        send_data(host, port)
        time.sleep(check_interval)

def initiate_threads(hosts, default_port):
    threads = []
    for host in hosts:
        threads.append(Thread(target=send_data_loop, args=(host,default_port)))

    for thread in threads:
        thread.start()
    for thread in threads:
        thread.join()

if __name__=='__main__':
    default_port = 12345
    args = register_parser()
    atexit.register(cleanup)

    initiate_threads(args.hosts, default_port)
    

