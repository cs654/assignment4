#!/usr/bin/python

import argparse
import socket
import atexit
import datetime
import time

def cleanup(sock):
    print("Closing current socket...")
    sock.close()
    print("Bye.")


def echo_data(host, port):
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.bind((host, port))
    s.listen(1)
    atexit.register(cleanup, s)

    while True:
        conn, addr = s.accept()
        try:
            while True:
                data = conn.recv(1024)
                if data:
                    if data.decode() == 'ping':
                        conn.send('echo'.encode())
                        print('[%s] Echo sent to %s.' 
                            % (datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')
                            , addr[0]))
                    else:
                        print(data.decode())
                        print('[%s] Unkown message received from %s.' 
                            % (datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')
                            , addr[0]))
                else:
                    conn.close()
                    break
        except:
            conn.close()

def register_parser():
    parser = argparse.ArgumentParser(description='Echo upon being pinged.')
    parser.add_argument('host', metavar='HOST', type=str,
                       help='ip to bind server on')

    args = parser.parse_args()
    return args

if __name__=='__main__':
    default_port = 12345
    args = register_parser()
    ip = args.host
    echo_data(ip, default_port)
