#!/usr/bin/python

import argparse
import socket
import atexit
import datetime
import time
from threading import Thread

current_ip_list = {}
is_dynamic = False
check_interval = 0
fault_declare_time = 0

def cron_job():
    while True:
        time.sleep(check_interval)
        for ip in current_ip_list.keys():
            if time.time() - current_ip_list[ip] > fault_declare_time:
                print('[%s] Machine at %s is \033[1;31mFAULTY\033[0m.' 
                    % (datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')
                    , ip))
            else:
                print('[%s] Machine at %s is \033[1;32mAVAILABLE\033[0m.' 
                    % (datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')
                    , ip))

def cleanup(sock):
    print("Closing current socket...")
    sock.close()
    print("Bye.")


def recv_heartbeat(host, port):
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.bind((host, port))
    s.listen(100)
    atexit.register(cleanup, s)

    while True:
        conn, addr = s.accept()
        try:
            while True:
                data = conn.recv(1024)
                if data:
                    ip = data.decode()
                    if ip in current_ip_list.keys():
                        current_ip_list[ip] = time.time()
                        conn.send('OK'.encode())
                    elif  is_dynamic:
                        current_ip_list[ip] = time.time()
                        conn.send('OK'.encode())
                    else:
                        conn.close()
                        break
                else:
                    conn.close()
                    break
        except:
            conn.close()

def register_parser():
    parser = argparse.ArgumentParser(description='Listen hearbeats.')
    parser.add_argument('--dynamic', dest='is_dynamic', action='store_true',
                        help='automatically increase the machine list upon new heartbeats')
    parser.add_argument('--check_interval', dest='check_interval', nargs="?", type=int,
                        default=10,
                        help='check interval in seconds (default=10)')
    parser.add_argument('--fault_declare_time', dest='fault_declare_time', nargs="?", type=int,
                        default=10,
                        help='time duration to declare fault in seconds (default=10)')
    parser.add_argument('ips', metavar='IP', type=str, nargs='*',
                        help='list of ips to listen')
    parser.add_argument('host', metavar='HOST', type=str,
                        help='ip to bind server on. Can be empty with --dynamic flag')

    args = parser.parse_args()
    return args

def initiate_threads():
    t1 = Thread(target=cron_job, args=())
    t2 = Thread(target=recv_heartbeat, args=(ip, default_port))
    t1.start()
    t2.start()
    t1.join()
    t2.join()

if __name__=='__main__':
    default_port = 12345
    args = register_parser()
    ip = args.host
    is_dynamic = args.is_dynamic
    if is_dynamic == False:
        if len(args.ips) == 0:
            print('usage: A.py [-h] [--dynamic] [IP [IP ...]] HOST')
            exit(0)
        else:
            for client_ip in args.ips:
                current_ip_list[client_ip] = 0
    check_interval = args.check_interval
    fault_declare_time = args.fault_declare_time
    initiate_threads()