#!/usr/bin/python

import argparse
import socket
import atexit
import datetime
import time

send_interval = 10
open_sockets = []

def cleanup():
    print("Closing current sockets...")
    for sock in open_sockets:
        sock.close()
    print("Bye.")

def send_data(host, port, data):
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    open_sockets.append(s)
    try:
        s.connect((host, port))
        while True:
            try:
                s.send(data.encode())
                response = s.recv(1024)
                if response:
                    if response.decode() == 'OK':
                        print('[%s] Heartbeat(%s) sent to %s.' 
                            % (datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')
                            , data
                            , host))
                    else:
                        print("[%s] Unfamiliar reply received on heartbeat."
                            % (datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')))
                        break
                else:
                    s.close()
                    break                   
            except:
                break
            time.sleep(send_interval)
    except:
        pass

    print('[%s] Error in connection to %s.' 
        % (datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')
        , host))
    s.close()
    open_sockets.remove(s)

def register_parser():
    parser = argparse.ArgumentParser(description='Ping different machines to check availability.')
    parser.add_argument('--interval', dest='interval', nargs="?", type=int,
                        default=10,
                        help='heartbeat interval in seconds (default 10)')
    parser.add_argument('host', metavar='HOST', type=str,
                       help='this machine\'s IP')
    parser.add_argument('server', metavar='SERVER', type=str,
                       help='IP to send heartbeat')

    args = parser.parse_args()
    return args

def send_data_loop(host, port, data):
    while True:
        send_data(host, port, data)
        time.sleep(send_interval)

if __name__=='__main__':
    default_port = 12345
    args = register_parser()
    send_interval = args.interval
    atexit.register(cleanup)
    send_data_loop(args.server, default_port, args.host)
    

